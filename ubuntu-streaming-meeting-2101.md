# Ubuntu 20.04LTSを\\n私はこんな感じで\\n使っています

subtitle
:  Debianのバグをなんとなく眺める

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  Ubuntu Streaming Meeting 21.01

allotted-time
:  10m

theme
:  .

# お知らせ: 資料は公開済み

* この資料はRabbit Slide Showで公開済み
  * Ubuntu 20.04LTSを私はこんな感じで使っています - Debianのバグをなんとなく眺める
    * <https://slide.rabbit-shocker.org/authors/kenhys/ubuntu-streaming-meeting-2101/>

# プロフィール

![](images/profile.png){:relative-height="60" align="right"}

* Debianのほうからきました
  * ひよこDebian Developer
    * 2020/09になったばかり
    * twitter: @kenhys
* トラックポイント(**ソフトドーム派**)
* わさビーフ(**わさっち派**) わさぎゅー🐮派ではない


# 20.04 LTS・20.10のリリース

# Ubuntuをこんな感じに使っています

* Debianのバグを**なんとなく**眺める🧐

# バグの眺め方(Ubuntu)

![](images/launchpad.png){:relative-height="100"}

# バグの眺め方(Debian)

* <https://bugs.debian.org/>を検索

* <https://udd.debian.org/bugs/>
  * BSP view (bugs needing attention)

* ただし、まだ誰もみていないバグを眺めるには不向き🤔

# bugs.debian.orgを検索

![](images/bugs-debian-org.png)

# BSP view (bugs needing attention)

![](images/bsp-view.png){:relative-height="90"}

# fabre.debian.net

# fabre.debian.net

* **注:非公式なプロジェクトです**
  * 個人のファン活動みたいな位置づけ
* **Ubuntu 20.04LTS**で稼働
* unstableに影響するバグ🐞を追える
* コメントがないバグ💬を一覧できる

# 最近更新されたバグ一覧

![Recent bugs](images/fabre-recent.png){:relative-height="100"}


# コメントがまだついていないバグ一覧

![Untouched bugs](images/fabre-fixme.png){:relative-height="100"}

# fabre.debian.netの今後

* よく落ちるのをどうにかする
  * 1vCPU・メモリ1GBではきびしい
  * <https://fosshost.org/> の支援を受けられることになった
    * 移行によりリソースの問題は改善する見込み
* バグ表示の改善といい感じの検索を実現する

# さいごに


* Ubuntuのパッケージだけで抱えているパッチはupstreamにフィードバックするなりDebianにも(以下略)
